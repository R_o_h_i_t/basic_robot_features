import qi
import argparse
import sys

def main(session):

    ALSpeech = session.service('ALTextToSpeech')
    ALSpeech.say('Hello World')

    ALMotion = session.service('ALMotion')
    ALMotion.wakeUp()

    #initializing the service
    ALDialog = session.service("ALDialog")
    ALDialog.setLanguage("English")

    #Topic Content
    topicContent = ("topic: ~example_topic()\n"
                    "language: enu\n"
                    "concept:(food) [fruits chicken beef eggs]\n"
                    "u: (I [want 'would like'] {some} _~food) Sure! You must really like $1 .")

    #loading the topic
    topic_name = ALDialog.loadTopicContent(topicContent)

    print (topic_name)
    #Activating the loaded topic
    ALDialog.activateTopic(topic_name)

    #we have to subscribe only once
    ALDialog.subscribe('topic')

    try:
        raw_input("\nSpeak to the robot using the rules from just loaded .top file. Press Enter when finished:")

    finally:
        #stopping the dialog engine
        ALDialog.unsubscribe('topic')

        #deactivating the topic
        ALDialog.deactivateTopic(topic_name)
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ip", type=str, default = "127.0.0.1",help="Robot's IP address. If on a robot or a local Naoqi -use '127.0.0.1' which is the default value.")
    
    parser.add_argument("--port", type=int , default=9559, help="port number required")

    args = parser.parse_args()
    session = qi.Session()

    try:
        session.connect("tcp://{}:{}".format(args.ip, args.port))
    except RuntimeError:
        print("Cannot connect to the Naoqui IP {} (port{})".format(args.ip, args.port))
        sys.exit(1)
    main(session)

