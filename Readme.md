THIS CONTAINS BASIC FEATURES ON HOW TO PERFORM THE BASIC FUNCTIONALITIES OF THE ROBOT

*******
TO USE THE FILE, enter the following command in the terminal
*******

python (name-of-the-file) --ip (ip-address-of-the-robot) --port (port-you-want-to-use)

example: python Speech_Recognition2.py --ip 192.168.1.21 --port 9559

Face_Detection.py:
    This has basic implementation of face-detection

Image_Capture.py:
    This has basic implementation of image capture with the help of the robot. This saves the image in desktop

Speech_Recognition.py:
    This uses 'ALDialog' API to make a conversation with the robot.
    This also uses basic function of pronuncing a text with the help of 'ALTextToSpeech' API.

Speech_Recognition2.py:
    This uses 'ALSpeechRecognition' API to recognize the words to trigger a callback function.

People_detection.py
    This uses 'ALPeoplePerception' API to track the people around the robot.

For more information on the API, visit : http://doc.aldebaran.com/2-5/getting_started/index.html