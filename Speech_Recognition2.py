import qi
import argparse
import sys
import time

def main(session):
  
    asr_service = session.service("ALSpeechRecognition")
    speech_service = session.service("ALTextToSpeech")
    memory_service = session.service("ALMemory")
    tracker_service = session.service("ALTracker")

    tracker_service.stopTracker()

    def callback(value):
        speech_service.say("hi")
        
    asr_service.setLanguage("English")

    vocabulary = ["yes", "no", "why"]

    asr_service.setVocabulary(vocabulary, False)

    asr_service.subscribe("Ro")

    print ("listening....")
    time.sleep(10)

    subscriber = memory_service.subscriber("WordRecognized")
    subscriber.signal.connect(callback)

    asr_service.unsubscribe("Ro")

#running the file with main method
if __name__ == "__main__":

    #adding arguments
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ip", type=str, default = "127.0.0.1",help="Robot's IP address. If on a robot or a local Naoqi -use '127.0.0.1' which is the default value.")
    parser.add_argument("--port", type=int , default=9559, help="port number required")

    args = parser.parse_args()
    session = qi.Session()

    try:
        session.connect("tcp://{}:{}".format(args.ip, args.port))
    except RuntimeError:
        print("Cannot connect to the Naoqui IP {} (port{})".format(args.ip, args.port))
        sys.exit(1)
    main(session)