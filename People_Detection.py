import qi
import argparse
import sys
import time
from naoqi import ALProxy
from PIL import Image

def main(session):

    # IP = "192.168.1.11"
    # PORT = 9559


    # imageProxy = ALProxy("ALVideoDevice", IP, PORT)

    # memoryProxy = ALProxy("ALMemory", IP, PORT)


    # subscriber = imageProxy.subscribe('Ro', 1, 11, 5)

    # image = imageProxy.getImageRemote(subscriber)

    # image_width = image[0]
    # image_height = image[1]
    # array = image[6]

    # image_string = str(bytearray(array))

    # im = Image.frombytes("RGB", (image_width, image_height), image_string)

    # im.save("/home/rohit/Desktop/test", 'PNG')
    # #print(image)
    # imageProxy.unsubscribe(subscriber)

    memory = session.service('ALMemory')
    motion = session.service('ALMotion')
    people = session.service('ALPeoplePerception')
    player = session.service('ALAudioPlayer')

    face_detection = session.service('ALFaceDetection')
    id = 1 #get Id
    logger = qi.Logger('PeopleCollector_{0}'.format())
    people_location = {}

    known_people = []

    def collect_people_position(self):
        people_location = {}

        try:
            detected_people = self.memory.getData('PeoplePerception/VisiblePeopleList')

            for people_id in detected_people:
                cur_pos = self.memory.getData()

                self.logger.info("Detected person with ID {0} in ")
                x,y,_ = cur_pos

                if x < DIST_THRESHOLD:
                    self.people.resetPopulation()
                    return True

                self._people_locations[people_id] = cur_pos

        except RuntimeError:
            return False

    def get_detected_people(self):
        
#running the file with main method
if __name__ == "__main__":

    #adding arguments
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ip", type=str, default = "127.0.0.1",help="Robot's IP address. If on a robot or a local Naoqi -use '127.0.0.1' which is the default value.")
    parser.add_argument("--port", type=int , default=9559, help="port number required")

    args = parser.parse_args()
    session = qi.Session()

    try:
        session.connect("tcp://{}:{}".format(args.ip, args.port))
    except RuntimeError:
        print("Cannot connect to the Naoqui IP {} (port{})".format(args.ip, args.port))
        sys.exit(1)
    main(session)