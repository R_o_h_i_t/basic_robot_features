
import qi
import argparse
import sys
import time
from naoqi import ALProxy

def main(session):

    # Replace this with your robot's IP address
    IP = "192.168.1.11"
    PORT = 9559
    # Create a proxy to ALFaceDetection
    try:
        faceProxy = ALProxy("ALFaceDetection", IP, PORT)
        
    except Exception, e:
        print "Error when creating face detection proxy:"
        print str(e)
        exit(1)

    # Subscribe to the ALFaceDetection proxy
    # This means that the module will write in ALMemory with
    # the given period below
    period = 500
    faceProxy.subscribe("Test_Face", period, 0.0 )

    # ALMemory variable where the ALFaceDetection module
    # outputs its results.
    memValue = "FaceDetected"

    # Create a proxy to ALMemory
    try:
        memoryProxy = ALProxy("ALMemory", IP, PORT)
        
    except Exception, e:
        print "Error when creating memory proxy:"
        print str(e)
        exit(1)

    # A simple loop that reads the memValue and checks whether faces are detected.
    for i in range(0, 20):
        time.sleep(0.5)
        val = memoryProxy.getData(memValue, 0)
        print ""
        print "\*****"
        print ""

    # Check whether we got a valid output: a list with two fields.
    if(val and isinstance(val, list) and len(val) == 2):
        # We detected faces !
        # For each face, we can read its shape info and ID.
        # First Field = TimeStamp.
        timeStamp = val[0]
        # Second Field = array of face_Info's.
        faceInfoArray = val[1]

        try:
        # Browse the faceInfoArray to get info on each detected face.
            for faceInfo in faceInfoArray:
            # First Field = Shape info.
                faceShapeInfo = faceInfo[0]
                # Second Field = Extra info (empty for now).
                faceExtraInfo = faceInfo[1]
                print "  alpha %.3f - beta %.3f" % (faceShapeInfo[1], faceShapeInfo[2])
                print "  width %.3f - height %.3f" % (faceShapeInfo[3], faceShapeInfo[4])
        except Exception, e:
            print "faces detected, but it seems getData is invalid. ALValue ="
            print val
            print "Error msg %s" % (str(e))
    else:
        print "Error with getData. ALValue = %s" % (str(val))
    
    # Unsubscribe the module.

    faceProxy.unsubscribe("Test_Face")
    print "Test terminated successfully."

#running the file with main method
if __name__ == "__main__":

    #adding arguments
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ip", type=str, default = "127.0.0.1",help="Robot's IP address. If on a robot or a local Naoqi -use '127.0.0.1' which is the default value.")
    parser.add_argument("--port", type=int , default=9559, help="port number required")

    args = parser.parse_args()
    session = qi.Session()

    try:
        session.connect("tcp://{}:{}".format(args.ip, args.port))
    except RuntimeError:
        print("Cannot connect to the Naoqui IP {} (port{})".format(args.ip, args.port))
        sys.exit(1)
    main(session)